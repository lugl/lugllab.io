

var ZIP_PROXY      = "PROXY 192.168.3.10:8123";		// komprimierender Proxy auf meinem VPN-Gateway
var LOCAL_PRIVOXY  = "PROXY 127.0.0.1:8118";		// eigener Rechner mit privoxy	
var FREETZ_PRIVOXY = "PROXY 192.168.179.1:8118";  	// fritzbox mit "Freetz" + "privoxy"

var BLOCKING_PROXY = "PROXY 127.1.2.4:888";
	// ungültige nicht erreichbare Adresse, das ist ein Hack
	

// wenn man verschiedene Proxies zur Auswahl hat, welcher soll es sein
var MY_PROXY = "DIRECT";		// ohne zusätzlichen Proxy
// var MY_PROXY = FREETZ_PRIVOXY;	// mit "privoxy" auf der Fritzbox
// var MY_PROXY = LOCAL_PRIVOXY;	// mit "privoxy" local 
// var MY_PROXY = ZIP_PROXY;		// mit "privoxy" auf VPN-Gateway


var BLOCKED = BLOCKING_PROXY;
	// return "BLOCKED"  hat früher mal funktioniert
	// jetzt brauchen wir einen Proxy der den Zugriff auf ungewünschte Ziele wirklich 
	// blockiert, indem er einfach nicht erreichbar ist


// IP-Adressen von zu administrierenden Geräten im eigenen Netz
var direct_hosts = [
	 '192.168.100.8'
	,'192.168.8.157'
];

// Zugriff erlaubt für URLs von vertrauenswürdigen Servern (ohne logging)
var whitelist_hosts = [
	 'localhost'
	 
/* konfiguration vom privoxy */
	,'p.p'
	,'config.privoxy.org'

/* Beispiel Wikipedia */	
/* Wikipedia ist die erste Webseite, die richtig voll funktioniert */
	,'.*wikipedia.(de|org)'
	,'(stats|meta|upload|login|intake-analytics|commons|www|species).wikimedia.(de|org)'
	,'wikimedia.org'
	,'en.wiki(source|books|news|versity).org'
	,'creativecommons.org'
	,'ocsp.starfieldtech.com'   // certificate validation
	
/* Karten */
	,'.*opentopomap.org'
	,'.*osm.org'
	,'.*openstreetmap.org'	
	
/* noscript */	
	,'.*noscript.net'
	,'.*addons.mozilla.org'
	,'.*creativecommons.org'	
];

// Zugriff generell nicht erlaubt für URLs von nicht-vertrauenswürdigen Servern (ohne logging)
var blacklist_hosts = [
         '.*mozilla.(com|net|org)'   /* erste Zeile ohne vorangestelltes Komma*/	
	,'.*firefox.(com|net|org)'
	,'push.*\.com'
	,'.*analytics.*'
];


function regExMatch(reg, host)
{
	var rex = new RegExp('^'+reg+'$',"igm");
//	alert(rex.source + "  host:["+ host +"]");
	var i = rex.test(host);
	return i;
}

function logResult(retVal, msg, host, url, dbg)
{
	if (dbg) {
		alert(msg+" url:"+url+" host:"+host);
	}
	return retVal;
}

function FindProxyForURL(url, host)
{
//	var myIP = myIpAddress();  alert("myIP: "+ myIP);

	if (url == 'https://[ff00::]/') {
		return BLOCKED;
	}

//	alert("test direct");
	for (i=0; i<direct_hosts.length; i++) {
		if (regExMatch(direct_hosts[i], host)) {
			return logResult("DIRECT", "direct access", host, url, false);
		}	
	}
	
//	alert("test whitelisted");
	for (i=0; i<whitelist_hosts.length; i++) {
		if (regExMatch(whitelist_hosts[i], host)) {
			return logResult(MY_PROXY, "whitelisted", host, url, false);
		}	
	}		
	
//	alert("test blacklisted");
	for (i=0; i<blacklist_hosts.length; i++) {
		if (regExMatch(blacklist_hosts[i], host)) {
			return logResult(BLOCKED, "blacklisted", host, url, false);
		}	
	}	
			
	// noch nicht gelisteter host, log-Eintrag in Browser Konsole schreiben
	return logResult(BLOCKED, "finally blocked" , host, url, true);
}


