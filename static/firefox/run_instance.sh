#!/bin/bash

profile=`basename $0`
if [ "${profile}" = "run_instance.sh" ]
then
	profile=""
fi

case $profile in 
	Sourceforge+Git)	EXE="/usr/lib/firefox-esr/firefox-esr"
				;;
			*)	export MOZILLA_HOME=`dirname $0`
				export LD_LIBRARY_PATH=${MOZILLA_HOME}
				BIN="firefox-bin"
				EXE=${MOZILLA_HOME}/${BIN}
				if ! test -x ${EXE}
				then
					echo "file [${EXE}] is not exectuable" >&2
					exit 1
				fi
				;;
esac


echo "profile \"${profile}\" exe \"${EXE}\""

# echo ${LD_LIBRARY_PATH}/${BIN} -P `basename $0`
# ldd ${LD_LIBRARY_PATH}/${BIN}
exec ${EXE} --no-remote --new-instance  -P "${profile}" &

