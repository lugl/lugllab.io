---
date: "2018-07-24T15:11:03+02:00"
title: "@LÄUFT"
authors: ["mr"]
emoji: true
categories:
  - Party
tags:
    - Feinstaubsensor
    - Workshop
---
Am 10. Juli trafen sich 6 Leipziger Linux User in der [dezentrale](https://dezentrale.space).


Der Plan sah vor den Zusammenbau, das Einrichten und die Inbetriebnahme der Feinstaubsensoren.

Unsere Gastgeber empfingen uns zunächst mit Fragen wie „Warum heute...“, das sich dann aber schnell aufklärte.
Unsere Anmeldung kam nicht an, was aber kein Problem war. Es fanden sich 4 Tische auf denen wir uns einrichten konnten.

### Die Bestandsaufnahme
Zunächst breitete jeder aus, was bei seiner Bestellung angekommen war. Grundlage der Bestellung waren dabei die Angaben auf [luftdaten.info](https://luftdaten.info/) . Die Bestellungen sind alle vollständig eingetroffen.

*   Kabelbinder
*   Schrumpfschlauch
*   Insektennetz
*   Ansaugschlauch
*   ein passendes Rohr zum absägen :wink:
{{< figure link="/feinstaub/LUG-L_Feinstaubsensor-07.jpg" thumb="-small" caption="Rohr">}}

wurden gespendet bzw. im „Großeinkauf“ besorgt.


### Das Werkzeug
Das wurde positiv von der dezentrale angemerkt. Wir hatten unser Werkzeug dabei! :v: OK, nicht alles, eine Säge und eine Schere hatte keiner der LUG eingepackt. Beides fand sich – Danke dezentrale.
Eine 3-fach Steckdose reichte uns für das umgebauten AT-Netzteil {{< figure link="/feinstaub/LUG-L_Feinstaubsensor-05.jpg" thumb="-small" caption="AT Netzteil">}}zur 5V Versorgung der Sensoren und den Lötkolben.

### Die Vorbereitung
Es war ein Spaziergang. Dank der Erfahrungswerte von SF. Ohne seine Tipps, Tricks und Kniffe hätten wir uns sehr schwer getan. Besonders das Löten der Verbindungskabel wäre schief gegangen. {{< figure link="/feinstaub/LUG-L_Feinstaubsensor-11.jpg" thumb="-small" caption="Löten">}}

### Der Zusammenbau
Plug and Play, nicht ganz. Das Zusammenstecken, Verbinden der einzelnen Bauteile zum Feinstaubsensor war dank der Anleitung von SF kein Problem.
{{< figure link="/feinstaub/LUG-L_Feinstaubsensor-09.jpg" thumb="-small" caption="Zusammebau">}}
Die Software spielte [DEX](/authors/dex) auf, denn nicht jeder hatte sein Laptop dabei. :scream: Angeschlossen an die Spannungsversorgung blinkten alle LED’s der Bauteile und gaben ihre Signale ab.

### Die Verpackung
Auch wieder kein Problem. Einsetzen, zusammenstecken und fertig. Durch die Winkel der Rohre fällt die Messeinheit auch nicht raus. Aber Tierchen besuchen unseren Feinstaubsensor gerne. Ist ja auch schön, trocken im Rohr. Mit einem Stück passendem Rohr, {{< figure link="/feinstaub/LUG-L_Feinstaubsensor-02.jpg" thumb="-small" caption="Fast fertig">}} über das ein Insektennetz gespannt wird und dies auf die Enden der Bögen geschoben wird, versperren wir den Eingang.
Zurück zur Überschrift. Wie „ät läuft“ ist hier zu sehen. Es wurden zusammen vier Messstationen erfolgreich zusammengebaut. Deren Messergebnisse ihr hier in Echtzeit sehen könnt.


### Messstation A
![PM10](https://www.madavi.de/sensor/images/sensor-esp8266-14120697-sds011-1-day.png)
![PM2.5](https://www.madavi.de/sensor/images/sensor-esp8266-14120697-sds011-25-day.png)

### Messstation B
![PM10](https://www.madavi.de/sensor/images/sensor-esp8266-1593245-sds011-1-day.png)
![PM2.5](https://www.madavi.de/sensor/images/sensor-esp8266-1593245-sds011-25-day.png)

### Messstation C
![PM10](https://www.madavi.de/sensor/images/sensor-esp8266-100011-sds011-1-day.png)
![PM2.5](https://www.madavi.de/sensor/images/sensor-esp8266-100011-sds011-25-day.png)

## Experimenteller Sensor
![PM10](https://www.madavi.de/sensor/images/sensor-esp8266-1594232-sds011-1-day.png)
![PM2.5](https://www.madavi.de/sensor/images/sensor-esp8266-1594232-sds011-25-day.png)

### Danksagung
*   Der dezentrale für die Gastfreundschaft.
*   Lothar für die Bilder.
*   Sebastian für die Anleitung beim Zusammenbau.


### Alle Bilder
{{< gallery dir="/feinstaub" thumb="-small"/>}}`

{{< load-photoswipe >}}
