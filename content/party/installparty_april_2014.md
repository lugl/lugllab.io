---
title: "Installparty am 5. April 2014"
date: 2014-04-05T15:39:32+02:00
authors: ["msc", "mr", "dex"]
draft: false
categories:
  - Party
tags:
  - Windows XP
  - Hands-On
---
Am 8. April 2014 endet der Support für Windows XP.
Sie werden keinerlei Sicherheitsupdates mehr für ihren Rechner erhalten.
Da hilft auch keine "Schutzsoftware"!
Für ein Softwareupgrade wir ihre Hardware sicherlich zu alt sein!
Was machen Sie mit Ihrem alten vermeintlich unbrauchbaren Rechner?
Wir [verhelfen](/installparty2014.pdf) Ihrem alten Rechner am 5. April zu einem neuen und sicheren Leben.
[Flyer zum Ausdrucken](/installparty2014_print.pdf)
