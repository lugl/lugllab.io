---
date: "2019-03-23T12:39:24+01:00"
title: "Wir sind keine Bots"
authors: ["dex"]
emoji: true
categories:
  - Party
tags:
    - Bürgerrechte
    - Datenschutz
    - Demo
---
Am 23. März 2019 trafen sich einige Mitglieder der Leipziger Linux User Group,
um an der Demo gegen die zur Abstimmung stehende Urherberechtsreform im EU
Parlament, insbesondere Artikel 13 zu demonstrieren.

### Danksagung
*   Dem Bündnis "Save the Internet" für die Organisation der Demo
*   Lothar für die Bilder.
*   Micha für die Organisation des Treffens


### Alle Bilder
{{< gallery dir="/keineBots" thumb="-small"/>}}`

{{< load-photoswipe >}}
