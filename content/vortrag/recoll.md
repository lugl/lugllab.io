---
title: "Recoll/Xapian - effiziente Dokumenten- / Desktopsuche"
date: 2016-01-26T19:30:00+02:00
authors: ["msc"]
draft: false
categories:
  - Vortrag
tags:  
  - Xapian
  - Recoll
  - Desktopsuche
---
[Vortrag](/vortrag/recoll.pdf) vom 26. Januar 2016
