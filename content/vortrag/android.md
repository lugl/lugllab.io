---
title: "Bürgerrechte bewahren - trotz Smartphone"
date: 2015-05-26T19:30:00+02:00
authors: ["msc"]
draft: false
categories:
  - Vortrag
tags:  
  - Android  
  - Bürgerrechte
  - Smartphone
  - Google
---
[Vortrag](/vortrag/android.pdf) vom 26. Mai 2015
