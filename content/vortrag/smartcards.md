---
title: "Smartcards - Crypto für die Hosentasche"
date: 2015-08-25T19:30:00+02:00
authors: ["msc"]
draft: false
categories:
  - Vortrag
tags:  
  - Smartcards
  - Crypto
---
[Vortrag](/vortrag/smartcards.pdf) vom 25. August 2015
