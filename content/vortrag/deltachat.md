---
date: "2024-05-28T19:30:00+02:00"
title: "Kommunikationsweg via DeltaChat erfolgreich migriert"
authors: ["mr"]
draft: false
categories:
  - Vortrag
tags:
  - Kommunikation
  - DeltaChat
  - Mail
---
Seit 2021 kannst du mit der Leipziger Linux User Group Kontakt über die DeltaChat Gruppe aufnehmen.

Um eine solche Gruppe einrichten zu können ist eine, nennen wir es mal „Basis-Mail-Adresse“ notwendig. Unter dieser Mail-Adresse kann dann eine Gruppe eingerichtet/eröffnet werden.

Bisher lief diese Gruppe über eine private Mail-Adresse bei [GMX](https://providers.delta.chat). Anfänglich problemlos! Welche Veränderungen auch immer dazu geführt haben, ist nicht nachzuvollziehen, jedoch wurde der „Basis-Mail“ Inhaber seit gut einem halben Jahr mit allen Beiträgen, die in DeltaChat und seinen verbundenen Bridge's abgesetzt wurden dreifach geflutet. Natürlich im DeltaChat-Client wo die Nachrichten auch hingehören aber auch als Mail im Postfach und zu jeder Nachricht noch Meldungen wie unzustellbar, verschlüsselt, autorisiert, ...! Ärgerlich, und für einen einfachen User nicht einfach nachzuvollziehen.

Seit Jahresbeginn werden zunehmend die [ChatMail](https://delta.chat/de/chatmail)-Server von DeltaChat als stabil bezeichnet. Dies hat die Möglichkeit eröffnet über eine dort angelegte „Basis-Mail-Adresse“ eine Gruppe zu eröffnen.

Hierzu wurde zunächst ein Test gestartet und über die Mailingliste zur Teilnahme aufgefordert. Mit 7 Teilnehmern lief der Test rund – Danke allen Teilnehmenden. Als irritierend stellte sich nur die Namensgebung der zufällig erstellten Mail-Adresse vor dem @ heraus. Hierzu fand sich eine deutschsprachige Beschreibung im [Forum](https://support.delta.chat/t/fehlermeldung-beim-versuch-delta-chat-zu-verwenden/2992/10).

In der „alten“ DeltaChat-Gruppe wurde eine Woche zuvor auf den Umzug in die neue Gruppe hingewiesen. Dies zusammen mit dem Einladungslink zur neuen [Gruppe](https://i.delta.chat/#6FE1642916908F1AC9CC7557CC99CF5DDB92043C&a=groupsbot%40testrun.org&g=LUG%F0%9F%90%A7Le&x=n8JI3_PpQSM&i=K5yQX8il3tI&s=-8jLuOcTOEn).

Der Mai Stammtisch war nun die Gelegenheit, ausstehende Verifizierungen zur Ende-zu-Ende-Verschlüsselung vorzunehmen. Probleme, die beim Zutritt zu der neuen DeltaChat Gruppe auftraten, zu klären und Meldungen auf Nachrichten zu erläutern. Hier nun, was bei diesem Vorhaben so alles vorkam. Es lagen dazu 7 Handys auf dem Tisch von denen zwei Teilnehmer der neuen Gruppe bereits angehörten und ein Teilnehmer DeltaChat nicht kannten.

* Der Beitritt zur Gruppe mit privaten Gmail- und iCloud-Konten kam nur sehr - sehr - sehr verzööööööögert zustande. Bei dem Empfang von Nachrichten aus der Gruppe war auch Geduld gefragt. Beide Teilnehmer entschlossen sich dann zu einer Mail-Adresse von einem der ChatMail Server. Dies gelang auch mit Hilfe und zeigte die schwächen der DeltaChat Anleitung auf. Fragen ergaben sich auch zur Auswahl der bereitgestellten [DeltaChat](https://delta.chat/de/chatmail)-Server Der Client verführt auf nine.testrun.org aber warum und welche Vor- und Nachteile bieten die jeweils bereitgestellten Server außer der Ansprache?
* Zwei Teilnehmer mit eigener E-Mail-Domain fanden keinen Zugang zur Gruppe. Vermutet werden SPF bzw. DKIM Einstellungen. Diese wurden mittlerweile behoben.
* Weitere Teilnehmer mit eigener Domain hatten keine Probleme.
* Alle Bridge's und der Invite-Bot tun ihren Dienst

Der Abend brachte Aufgaben für die Zukunft.

* Einrichten der Matrix ⇿ DeltaChat Bridge.
* Einbindung der Mailingliste in die Chat basierte Kommunikation der LUG-LE.
* Betrieb und Angebot eines eigenen ChatMail-Servers analog zur Mailingliste.
* Einrichten einer RSS Bridge, Homepage ⇾ DeltaChat/telegram/matrix

weiterführende Links:

* Einstieg: https://delta.chat/de ; https://delta.chat/de/chatmail
* Übersicht: https://cosmos.delta.chat
* Forum: https://support.delta.chat/c/de