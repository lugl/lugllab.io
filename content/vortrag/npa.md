---
title: "Der neue Personalausweis - Technik, Chancen, Risiken"
date: 2015-09-29T19:30:00+02:00
authors: ["rm"]
draft: false
categories:
  - Vortrag
tags:  
  - Smartcards
  - Crypto
---
[Vortrag](/vortrag/npa.pdf) vom 29. September 2015
