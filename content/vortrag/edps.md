---
title: "European Data Protection Supervisor"
date: 2017-06-20T19:30:00+02:00
authors: ["rm"]
draft: false
categories:
  - Vortrag
tags:  
  - EDPS
  - WPIA
  - IPEN
---
[EDPS](https://edps.europa.eu/) - European Data Protection Supervisor - setzt sich mit diesem Themenkreis auseinander. Im Anschluss an ihren diesjährigen Workshop findet in Wien ein Workshop von [IPEN](https://edps.europa.eu/data-protection/ipen-internet-privacy-engineering-network_en) - Internet Privacy Engineering Network - statt. Reinhard Mutz hat die Gelegenheit erhalten, dort den Verein [WPIA](https://wpia.club/de/) und dessen Trust Modell vorzustellen. An unserem Stammtisch wird Reinhard über seine Erlebnisse sowie neue Erkenntnisse und vielleicht auch neue technische Ansätze zu den Themen berichten.
