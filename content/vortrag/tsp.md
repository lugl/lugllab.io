---
title: "Why we need a legal framework to operate a Trusted Service Provider successfully"
date: 2017-01-31T19:30:00+02:00
authors: ["rm"]
draft: false
categories:
  - Vortrag
tags:  
  - Trusted Service Provider
  - Crypto
  - FOSDEM
---
Vortrag vom 31. Januar 2017; Preview zur [Fosdem](https://fosdem.org/2017/schedule/event/trusted_service_provider/)
