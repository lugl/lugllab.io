---
date: "2018-05-29T19:30:00+02:00"
title: "Statische Seiten Generierung am Beispiel der LUG-L Homepage"
authors: ["dex"]
draft: false
categories:
  - Vortrag
tags:
  - Hugo
  - Homepage
  - Staticgen
---
[Vortrag](/vortrag/staticgen.pdf) vom 29. Mai 2018
