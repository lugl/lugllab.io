---
date: "2021-09-30T17:25:20+02:00"
title: "Kommunikation ist -fast- alles"
authors: ["mr"]
categories:
  - Vortrag
tags:
  - Kommunikation
  - LUG
  - Matrix
  - Telegram
  - E-Mail
  - RSS
  - Pleroma
draft: false
---
# Kommunikation ist *fast* alles!

Der erfolgreichste da nachhaltigste Weg sich auszutauschen, ist immer noch die persönliche, direkte Begegnung. Dank niedriger Inzidenzzahlen in Leipzig können wir \o/ am letzten Donnerstag im Monat zu unserem [Stammtisch](/page/treffen/) einladen.


Für all die Gründe, die gegen den monatlichen geselligen ~~Abend~~ Austausch sprechen, halten wir die  Informationswege Homepage, RSS-Feed und Pleroma bereit. Darüber hinaus und für den Austausch gibt es unsere Mailingliste, einen Matrix-Raum und einen Telegram-Kanal, der via Bridge angebunden ist.
Der folgende Vortrag wird die bestehenden Kommunikationswege der LUG-L visualisieren und mögliche Verknüpfungen zwischen modernen Chatanwendungen und dem Jahrzehnte alten E-Mail-Verteiler beschreiben.

[Vortrag](/vortrag/Praesi_Komm-LUG-LE-Webseite.pdf) vom 30. September 2021