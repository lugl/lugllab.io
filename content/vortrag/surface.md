---
title: "Linux Installation auf Surface RT"
date: 2024-09-24T19:30:00+02:00
authors: ["krl"]
draft: false
categories:
  - Vortrag
tags:  
  - Surface RT
  - Elektroschrott Vermeidung
  - Jailbreak
---
[Vortrag](/vortrag/Lee_SurfaceRT.pdf) vom 24. September 2024
