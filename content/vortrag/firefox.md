---
date: "2023-02-13T20:14:41+01:00"
title: "Sicherer im Intenet durch Profile in Firefox"
authors: ["tp"]
categories:
  - Vortrag
tags: 
  - Sicherheit
  - Firefox
draft: false
---

## Materialien
* [Stichpunktliste/Gedankenstütze](/firefox/Firewall-im-Firefox.pdf)  für den Vortragenden
* [No-Script](/firefox/noscript-11.4.15.xpi) Firefox-Plugin
* [Beispieldatei](/firefox/alacarte-made-20.desktop) für Menüeintrag im Desktop, ins Verzeichnis $HOME/.local/share/applications kopieren und editiern

* [Beispiel](/firefox/startseite.html) für eine leere Startseite um das aktuell benutzte Profil anzuzeigen
* [Beispieldatei](/firefox/run_instance.sh) für Startskript bei Verwendung verschiedener Firefox-Versionen, im Firefox-Installationsverzeichnis einen symbolischen Link mit Namen des gewünschten Profils auf dieses Script anlegen:

{{< highlight bash >}}
cd /opt/firefox/62.0.3
ln -s /opt/firefox/run_instance.sh PROFILNAME
{{< / highlight >}}

* minimales [Proxy-Script](/firefox/proxy-pac_empty.js) mit einer einzigen funktionierenden Webseite "Wikipedia"
* mit mehreren Einträgen erweitertes [Proxy-Script](/firefox/proxy-pac_surfen.js)
* Grafik [OSI-Schichten-Modell](/firefox/OSI-Modell.eps) Netzwerkkommunikation
* Grafik [OSI-Schichten-Modell](/firefox/proxy.eps) "Was ist/macht ein Proxy"


### Bildschirm-Fotos vom Vortrag
{{< gallery dir="/firefox" thumb="-small"/>}}`