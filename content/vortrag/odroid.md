---
title: "Linux auf ARM und i386 - ein Vergleich"
date: 2015-04-28T19:30:00+02:00
authors: ["ab"]
draft: false
categories:
  - Vortrag
tags:  
  - ODROID
  - ARM
---
[Vortrag](/vortrag/odroid.pdf) zum [ODROID](http://odroid.com/dokuwiki/doku.php?id=en:odroid-u3) vom 28. April 2015
