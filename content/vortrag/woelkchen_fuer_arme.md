---
title: "Wölkchen für ARMe"
date: 2015-01-27T19:30:00+02:00
authors: ["jp"]
draft: false
categories:
  - Vortrag
tags:  
  - OwnCloud
  - ARM
---

OwnCloud auf ARM-basiertem Mini-NAS
[Vortrag](/vortrag/owncloud_arm2015.pdf) vom 27. Januar 2015
