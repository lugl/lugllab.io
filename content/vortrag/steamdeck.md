---
date: "2023-09-26T17:30:08+02:00"
title: "Valve Steam Deck"
authors: ["krl"]
categories:
  - Vortrag
tags:
  - Linux
  - Valve
  - Gaming
draft: false
---

### Bilder vom Votrag am 26. September 2023
{{< gallery dir="/steamdeck" thumb="-small"/>}}`

{{< load-photoswipe >}}