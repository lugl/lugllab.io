---
date: "2019-06-02T16:49:33+02:00"
title: "Der ODROID-N2"
authors: ["ab"]
draft: false
categories:
  - Vortrag
tags:  
  - ODROID
  - ARM
---
[Vortrag](/vortrag/Odroid_N2.odt) zum [ODROID-N2](https://wiki.odroid.com/odroid-n2/odroid-n2) vom 28. Mai 2019
