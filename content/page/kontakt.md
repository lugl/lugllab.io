---
title: Kontakt
slug: kontakt
comments: false
menu: main
weight: -100
---

Für die Kontaktaufnahme zu den Leipziger Linux Usern stehen mehrere Wege offen.
Sollten dir einige der Wege unbekannt sein, du dich aber dafür interessierst helfen wir im Rahmen unseres [Stammtisch](/page/treffen) weiter.

| | |
| --- | --- |
| Die meisten Mitglieder der Leipziger Linux User Group wirst du über die [Mailingliste](/page/mailingliste) ansprechen können.| [![Pinguin mit Papierflieger](/images/Penguin_mailman_by_mimooh.png "Mailingliste")](/page/mailingliste)  |
| Du möchtest dein Wissen teilen und selbst einen neuen Vortrag im Rahmen unseres monatlichen Stammtisches halten? Dann melde Dich unter <nobr>vortrag \[bei\] gaos.org</nobr> | [![Vorträge](/images/projektor.png "Projektor")](/categories/vortrag) |


Mit Hilfe von öffentlichen und kostenfreien Tools sind die bisher von uns genutzten [Messenger](https://www.kuketz-blog.de/die-verrueckte-welt-der-messenger-messenger-teil1)-Dienste teilweise über Bridges miteinander verbunden*.

Matrix :left_right_arrow: Telegram sowie DeltaChat :left_right_arrow: Telegram.

Für die Bridge DeltaChat :left_right_arrow: Matrix suchen wir noch nach einer stabilen, Ressourcenschonenden und kostenfreien Lösung.

| | |
| --- | --- |
| [DeltaChat](https://cosmos.delta.chat) ist eine spezielles "[Mail](https://providers.delta.chat)-Anwendung” welches eine Chatfunktion bereitstellt. DC ist die jüngste und einfachste Möglichkeit mit uns in Kontakt zu treten.  Installiere hierzu DC, richte DC mit einer persönlichen E-Mail-Adresse ein und scanne den QR-Code oder folge dieser [Einladung](https://i.delta.chat/#6FE1642916908F1AC9CC7557CC99CF5DDB92043C&a=groupsbot%40testrun.org&g=LUG%F0%9F%90%A7Le&x=n8JI3_PpQSM&i=K5yQX8il3tI&s=-8jLuOcTOEn) bei. | <a href="https://delta.chat"><img src="/images/deltachat.jpg" alt="DeltaChat" width="160"></a>.|
| Einige Leipziger Linux User haben sich in einer Telegram Gruppe zusammengefunden. Die schnelle Kontaktaufnahme ist in der Gruppe: LUG-Leipzig <nobr>[https://t.me/joinchat/VePvSgYOT4iIGwmb](https://t.me/joinchat/VePvSgYOT4iIGwmb)</nobr> möglich |<img src="/images/telegram_qr_code.jpg" alt="Telegram" width="160">|
|Last but not least findet sich auch bei Matrix ein Raum [https://matrix.to/#/#lug-leipzig](https://app.element.io/#/room/#lug-leipzig:matrix.org?via=matrix.org&via=t2bot.io&via=chat.dezentrale.space) mit Leipziger Linux Usern |[![Matrix Logo](/images/320px-Matrix_logo.svg.png)](https://matrix.org/)   |
