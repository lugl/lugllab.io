---
date: 2017-09-28T08:00:00+06:00
title: Links
slug: link
comments: false
menu: main
weight: -80
---

### Allgemeine Links
* [Linux User Group in der Wikipedia](https://de.wikipedia.org/wiki/Linux_User_Group)

### Linux User Groups in der Nachbarschaft

* [Bautzen](http://www.lug-bz.de)
* [Chemnitz](http://www.clug.de)
* [Delitzsch](http://www.lug-delitzsch.de)
* [Dresden](http://lug-dd.schlittermann.de)
* [Eichsfeld](http://linux.eichsfeld.net)
* [Eisenach](www.lug-eisenach.de)
* [Freiberg](http://www.flux.tu-freiberg.de)
* [Görlitz] (https://digitale-oberlausitz.eu/projekte/linux-stammtisch)
* [Gotha](https://www.tif-it.org)
* [Halle](http://www.halix.info)
* [Halle 2](http://hality.org)
* [Halberstadt](http://www.lug-hbs.de)
* [Heiligenstadt](http://linuxnode.eichsfeld.net)
* [Jena](http://www.lug-jena.de)
* [Magdeburg] (http://www.netz39.de)
* [Thüringer Linux User Group](http://www.tlug.de)
* [Wernigerode](http://www.lug-wr.de)
* [Zwickau](http://www.zlug.org)
* [Zwickau 2](https://zlabor.wordpress.com/)
* [Zittau](https://werkraum.freiraumzittau.de)