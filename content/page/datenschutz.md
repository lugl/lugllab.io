---
date: 2017-09-28T08:00:00+06:00
title: Datenschutz
slug: datenschutz
comments: false
menu: main
weight: -70
---

# Hinweise zum Datenschutz
##### Wie wir mit euren Daten umgehen
Datenschutz ist uns wichtig. Deshalb bekommt ihr hier eine Übersicht, welche personenbezogenen Daten von euch auf diesem Rechner wie lange gespeichert werden und wie ihr dafür sorgt, dass sie dort wieder verschwinden.

<!--more-->

## Webseiten
Der Webserver speichert bei jedem Aufruf Datum und Uhrzeit, die abgerufene URL, den Referer (die URL, von der aus die Seite aufgerufen wurde, falls vorhanden) und die Browseridentifikation des Zugriffs. Diese Daten werden zur Analyse und Abwehr von Angriffen ("berechtigtes Interesse" nach Art. 6 DSGVO) bis zu 30 Tage gespeichert und dann automatisch gelöscht.

Für das Web-Interface der Mailingliste gelten weitere Regeln, siehe weiter unten unter "Mailingliste".

## Kommentarfunktion
Die Kommentare inklusive aller optionlen Angaben wie Name, Email und Website werden lokal gespeichert.

## Mailingliste
Die Listenserver-Software Mailman, die die Mailingliste der LUG-L betreibt, speichert dauerhaft folgende personenbezogenen Daten:

Eure Mailadresse, die ihr für die Anmeldung an der Mailingliste benutzt habt.
Eure E-Mails, die ihr an die Liste geschickt habt, einschließlich aller Header.
Eure Aktionen bei der Verwaltung der Mailingliste (Anmelden, Abmelden, Einstellungen, Mails senden) mit eurer Mailadresse und einem Zeitstempel.
Auf diese Daten haben nur Administratoren Zugriff.
Wenn ihr euch auf der Mailingliste einschreibt, müsst ihr das Abo bestätigen. Die Bestätigung gilt dann als Einwilligung (Art. 7 DSGVO) in die Verarbeitung und Speicherung dieser Daten, ohne die wir die Mailingliste gar nicht anbieten könnten.

## Mailinglisten-archiv
Alle E-Mails an die LUG-L-Liste werden von Mailman verarbeitet und dauerhaft in einem Archiv gespeichert. Nur Listenteilnehmer und Administratoren haben Zugriff auf dieses Archiv. Es ist nicht öffentlich zugänglich.

Wenn ihr Mails an die LUG-L-Mailingliste schicken wollte, aber nicht wollt, dass diese Mails im Archiv gespeichert werden, dann könnt ihr eurem Mail User Agent beibringen, dass er den folgenden Header setzt:

`X-No-Archive: yes`

Damit werden eure Mails nicht auf dem Server archiviert. Dies verhindert nicht, dass andere Empfänger eure Mails speichern oder dass Mails archiviert werden, in denen andere eure Mails ganz oder in Ausschnitten zitieren!

## Weitere Dienste
Der Mailserver speichert für alle Mails, die an die LUG-L gehen, E-Mail-Adressen, IP-Adressen von einliefernden Servern sowie Zeitstempel der Einlieferung und Weiterleitung. Die Daten werden werden für vier Wochen gespeichert und dann automatisch gelöscht.

## Weitergabe von Daten
Eure Daten werden von uns an niemanden weitergegeben, außer es sollte sich einmal der Fall ergeben, dass wir von Rechts wegen dazu gezwungen wären. Das ist bisher nicht vorgekommen.

## Recht auf Löschung von Daten (art. 17 DSGVO)
Die meisten erhobenen personenbezogenen Daten werden, wie oben erläutert, automatisch innerhalb von maximal 30 Tagen gelöscht. Solltet ihr aber z.B. eure E-Mails nicht mehr länger im Archiv gespeichert haben oder andere Daten gelöscht haben wollen, dann könnt ihr uns kontaktieren und eure Einwilligung zur Speicherung eurer Daten widerrufen. Die Daten werden, soweit das technisch machbar ist, unverzüglich gelöscht. Ihr bekommt darüber eine Bestätigung.
