---
date: 2017-09-28T08:00:00+06:00
title: Treffen
slug: treffen
comments: false
menu: main
weight: -110
---

### Stammtisch

![Stammtisch Logo](/images/Tux_LUG-Leipzig_Stammtisch_v3.png)

Im allgemeinen jeden letzten Dienstag im Monat um 19:00 Uhr, außer im Dezember

<!--more-->

[![Termin zum Eintragen als ICS](/images/70278482-81d3-4aad-8de5-e20c424a0323.jpeg)](/LinuxUserGroupLeipzig.ics)

In der

```
Lutherburg
Wittenberger Straße 26
04129 Leipzig
```
https://lutherburg-leipzig.de/

![Geolocation als QR-Code](/images/GEO-Location-Lutherburg.png)




{{< load-leaflet >}}
{{< leaflet-simple mapHeight="500px" mapWidth="200" mapLon="12.3863340" mapLat="51.3620879" markerLon="12.3863340" markerLat="51.3620879" markerContent="Lutherburg Wittenberger Straße 26 04129 Leipzig" >}}

