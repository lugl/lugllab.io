---
date: 2017-09-28T08:00:00+06:00
title: Mailingliste
slug: mailingliste
comments: false
---
<!--more-->

### Archiv der Mailingliste der Linux User Group Leipzig
[Archiv der Mailingliste (Passwortgeschützt)](https://lists.gaos.org/pipermail/lug-l/)

### Die LUG-L Mailingliste abonnieren

 <form method="post" action="https://lists.gaos.org/cgi-bin/mailman/subscribe/lug-l">
  E-mail Addresse: <input name="email"> Name (optional):
  <input name="fullname">

  Hier kann ein Passwort gesetzt werden. Bitte *kein* wichtiges verwenden, da
  es im Klartext versendet wird, sollte es vergessen werden.

  <p>Passwort: <input type="password" name="pw"> Bestätigung: <input type="password" name="pw-conf"></p>

  <p>Sollen die Mails gebündelt versendet werden?<input type="radio" name=
  "digest" value="0" checked> Nein <input type="radio" name="digest"
  value="1"> Ja</p>

  <p><input type="submit" name="email-button" value=
  "Abonnieren"></p>
  </form>
