---
date: "2020-03-21T17:57:46+01:00"
title: "Stammtisch am 31.03.20 - in Zeiten von Corona"
authors: ["mr"]
emoji: true
categories:
  - Ankündigung
tags:
  - Stammtisch
  - Corona
  - Jitsi
---

Liebe Leipziger Linux User,


turnusmäßig würde der nächste Stammtisch am **31.03.2020** in der Lutherburg
stadtfinden. Die Lutherburg ist, ihr könnt es euch denken, geschlossen.


Lasst uns also am **31.03.2020** um 19:15 Uhr im Netz treffen. Neben der
guten Laune, die ihr sonst mitbringt ist für Getränke und Essen
selbst zu sorgen. Hinzu kommt ein wenig Vorbereitung damit wir
uns wenigstens hören können und so es das „Neuland-Netz“
hergibt, auch sehen werden. Und nein, wer teilnehmen will wird
ohne Installation, ohne Anmeldung, ohne Konto, ohne Bild von sich
und kostenfrei dabei sein können da [Jitsi](https://jitsi.org/) browserbasiert
funktioniert. 

![Jitsi Logo](/images/170px-Logo_Jitsi.svg.png "Jitsi Logo")

Damit unser 1. virtueller Stammtisch für alle erfolgreich wird:

* Die Browser Chrome bzw. Chromium, Opera bzw. Vivaldi sind erprobt. Die
Installation der Browser-Erweiterung kann je nach Konfiguration kontraproduktiv
sein. Ist der Browser aktuell?
* Ein Headset ist optimal Kopfhörer werden empfohlen.
* Wer mit dem Smartphone oder Tablet an der Konferenz teilnehmen will kann dies
über den Browser machen sollte aber die App installieren.
Auch hier werden Kopfhörer empfohlen.
  * [Android (Google Play)](https://play.google.com/store/apps/details?id=org.jitsi.meet)
  * [Android (FDroid)](https://f-droid.org/en/packages/org.jitsi.meet/)
  * [Apple](https://itunes.apple.com/us/app/jitsi-meet/id1165103905)
* Seid per Mail bzw. Matrix am 31.03. erreichbar. Darüber wird der Meeting-Link also
die URL zum Konferenzraum bekannt gegeben.
* Bitte, beginnt die Teilnahme am virtuellen Stammtisch zunächst nur mit Ton.
* Stellt die Video-Qualität auf „Niedrige Auflösung“=LD ein, wenn der Raum steht
kann Video hinzugeschaltet werden.


:exclamation: Es sind Rückkopplungen zu vermeiden, testet unbedingt vorher die Kamera, das
Mikrofon bzw. das Headset im Zusammenspiel mit dem Browser und eurem OS. Nutzt
dazu den [Referenz-Entwickler-Server](https://met.jit.si)

u.U. habt ihr an eure Firewall zu schrauben. :smile:
