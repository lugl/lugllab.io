---
date: "2022-11-09T19:29:41+01:00"
title: "Mach's gut, Lutz"
authors: ["mr"]
draft: false
emoji: true
categories:
  - Ankündigung
tags:
- Stammtisch
---
Am 27. Oktober haben wir nach 20 Jahren in der Lutherburg Lutz verabschiedet.

Lothar hatte eine Grußkarte besorgt, mit lieben Grüßen versehen und alle Anwesenden konnten diese dann unterschreiben. Bei einer kleinen Sammlung kam ein Sümmchen zusammen, welches nur raschelte \o/. 
{{< figure link="/images/photo_2022-11-10_19-19-55.jpg" thumb="-small" caption="Erwischt von Lothar">}}
So zwischen Tür und Angel und kurz vor Feierabend haben wir zusammen mit einem gereiften Traubensaft die Karte und den Umschlag übergeben. Das Bild spricht wohl für sich. Die Überraschung war gelungen.

Ein herzliches Dankeschön geht an den gesamten Stammtisch.

<HR>
{{< text s="0.7" color="grey" >}}
Ach so, wer sich fragt: Lutz wer? Er war in den letzten 20 Jahren auch unser Koch. Eine Bitte lässt er ausrichten. Wer immer Hilfe, auch handwerklicher Natur benötigt, darf sich gern an ihn wenden. Kontaktdaten gibt es über Lothar.
{{< /text >}}

{{< figure link="/images/photo_2022-11-10_19-19-35.jpg" thumb="-small" caption="Lothar hat sogar 2x geknips">}}


{{< load-photoswipe >}}