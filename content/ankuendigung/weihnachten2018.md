---
date: 2018-12-01T16:59:54+02:00
title: "IT-Hardware Schrott-Wichteln"
authors: ["mr"]
draft: false
emoji: true
categories:
  - Ankündigung
tags:
  - Weihnachtsfeier
  - Wichteln
---

### Kling, Glöckchen klingelingeling – oder was klappert da im Paket


![Weihnachtstux](/images/weihnachtstux.jpeg)
Ja, es ist schon wieder so weit. Das Jahr geht zur Neige und wie fast jedes Jahr müssen wir unseren Dezember Stammtisch verschieben.
Der neue Wirt der Lutherburg bat uns ja schon im September darum. Am „letzten Dienstag im Oktober“ fanden wir den Ausweichtermin. In diesem Jahr fällt der letzte Dienstag im Dezember auf den 1. Weihnachtsfeiertag. Die Mehrheit wollte den Dienstag gern beibehalten und da an diesem Tag in der Lutherburg keine Weihnachtsfeier ansteht, passte alles zusammen.

Wir treffen uns also am 18. Dezember zum Weihnachtswichteln in der Lutherburg.

Und damit bekommt die Überschrift auch ihren Sinn. Wir wollen wieder Wichteln. Nun gab es gleich zahlreiche Fragen zu dem Wie und Was und Wieviel und überhaupt. Im Netz finden sich zahlreiche Artikel zum Thema Wichteln, hier nur ein Link zur [Wikipedia](https://de.wikipedia.org/wiki/Wichteln).
Unser Wichteln läuft unter dem Begriff


<DIV align="center">
![Linie](/images/Linie20.gif)
## IT - Hardware – Schrott – Wichteln
Dienstag, 18. Dezember 2018</BR>
19.00 Uhr in der Lutherburg</BR>
</DIV>


Und so geht es: Du packst in ein Päckchen was immer du an PC oder sonstige IT Hardware abgeben kannst. Es können auch mehrere Teile sein. Es gibt nur eine Bedingung. Was immer du einpackst, darf „kein Schrott“ sein, muss also noch funktionieren.

Der Ablauf sieht so aus: Es wird einen Gabentisch geben. Auf diesem Tisch liegt ein Abreißblock mit doppelten Nummern und daneben steht unser Stammtisch-Aufsteller. Du heftest einen Zettel mit Nummer an deine Gabe und legst den anderen Zettel mit der gleichen Nummer in unseren Stammtisch-Aufsteller.</BR>
Jetzt wird es anstrengend, merke dir diese Nummer! ![Los](/images/lose.jpeg)


Im Laufe des Abends zieht jeder ein Los aus unserem Aufsteller, vergleicht die Nummer auf dem Zettel mit der Nummer in seinem Speicher und wenn ≠ behält er dieses Los. Sonst zieht er ein weiteres Los und legt das Los mit der Nummer = Speichernummer zurück in unseren Stammtisch-Aufsteller.
Mit der Losnummer ≠ Speichernummer greift man sich das Päckchen mit gleicher Nummer und das AAH und OOH kann beginnen.
<DIV align="center">
![Linie](/images/Linie20.gif)
</DIV>
