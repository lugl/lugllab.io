---
date: "2020-04-23T18:23:18+02:00"
title: "2. virtueller Stammtisch am 28.04.2020"
authors: ["mr"]
emoji: true
categories:
  - Ankündigung
tags:
  - Stammtisch
  - Corona
  - Big Blue Button
---

Hallo Leipziger Linux User,

wer hätte vor 5 Wochen gedacht, dass auch der April Stammtisch ins Netzt fällt. Und genau dort werden wir uns am **28.04.2020, um 19:15** Uhr erneut treffen.

Am Prozedere hat sich nichts geändert. Eure Hardware läuft, die Getränke sind kalt-, das Essen warmgestellt, die Laune könnte nicht besser sein :smile:, ihr seid online und per Mail bzw. per Martrix / Telegram zu erreichen. Kurz nach 19:00 Uhr, erhaltet ihr für diesen Abend, wieder einen Link zu unserem Raum. 
Diesmal werden wir uns, sicher zur Freude alle Firefox Nutzer, bei [BigBlueButton](https://bigbluebutton.org) einfinden.  

![Big Blue Button Logo](/images/photo_2020-04-23_18-43-45.jpg "Big Blue Button Logo")


Wie schon zuvor läuft alles ohne Anmeldung, Installation, Registrierung, Konto und wer mag auch ohne Bild.
Damit auch dieser Stammtisch wieder für alle Teilnehmer erfolgreich verläuft, gelten die gleichen technischen Empfehlungen wie zu unserem [ersten virtuellen Stammtisch](/ankündigungen/corona). 
Auch bei BigBlueButton ist alles intuitiv zu bedienen. Die Anleitung: [BigBlueButton als Teilnehmer](https://bildungsportal.sachsen.de/opal/auth/RepositoryEntry/516358148/CourseNode/101490883068104) kann jedoch nicht schaden.

Danke