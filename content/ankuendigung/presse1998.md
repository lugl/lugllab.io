---
title: "Presseinfo zur Installationsparty 1998"
date: 1998-10-04T00:00:00+02:00
draft: false
categories:
  - Ankündigung
---
# Presseinfo zur Installationsparty

Die Leipziger Linux User Group lädt zur ersten Linuxinstallationsparty in Leipzig.

Leipzig, 4.10.1998 Was als kleines Projekt eines finnischen Informatikstudenten begann, trat einen unvergleichlichen Siegeszug über den gesamten Globus an. Linux, ein Unix-ähnliches Betriebsystem, ist nicht länger die Einzelleistung des Erfinders Linus Torvalds. Seit 1991 griffen mehrere Hundert Programmierer die Idee des kostenlosen Betriebsystems für den PC auf und brachten die Entwicklung in einer unvergleichlichen, ehrenamtlichen Teamarbeit voran.

Inzwischen ist Linux eine ausgereifte und äußerst verläßliche Plattform für zahlreiche Rechnerarten (PC, Macintosh, Sparc, Amiga, Alpha uvm.) und insbesondere wegen seiner Netzwerkfähigkeit weit verbreitet. Aber auch immer mehr Anwender sehen in Linux, vor allem wegen dessen Stabilität, eine kostenlose Alternative zu den kommerziellen Entwicklungen wie Windows 95.

Leider gestaltet sich aufgrund der schnellen Weiterentwicklung eine Installation oftmals als schwierig. Aus diesen Grund wird am 24.Okt.1998 die 1. Leipziger Linux Installationsparty im Hauptgebäude der Universität (am Augustusplatz) ab 10 Uhr im Raum 4-24 stattfinden.

Veranstalter ist der Leipziger Linuxstammtisch, der bei seinen monatlichen Treffen zunehmend Interesse an dem neuen Betriebsystem erfuhr. Selbstverständlich ist der Eintritt und die Installationshilfe durch einen Experten vollkommen kostenlos. Um Anmeldung wird jedoch gebeten, da die Anzahl der Computerstellplätze begrenzt ist (im Internet: http://come.to/linux-party eMail: larisch@informatik.uni-leipzig.de Telefon: 0341/9732247 Fax: 0341/2325084 (Informationsblatt per Faxabruf)). Auch Neugierige, Kritiker und Profis sind gleichermaßen eingeladen sich zum Beispiel an den ausgestellten Rechnern oder in der Linux-Bücherecke ein Bild zu machen. Internetanschluss ist ebenfalls vorhanden.
