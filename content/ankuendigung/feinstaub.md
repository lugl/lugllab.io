---
title: "Feinstaubsensor im Eigenbau"
date: 2018-04-03T16:59:54+02:00
authors: ["mr"]
draft: false
emoji: true
categories:
  - Ankündigung
tags:
  - Feinstaubsensor
  - Vorbereitungstreffen
---

Das BVG-Urteil vom 27.02.2018 hat die Möglichkeit eröffnet, Fahrverbote auszusprechen.
Die Umsetzung hat nur einen Haken.
Es wird die selbe Stelle, die die Feinstaubbelastung misst, auch Fahrverbote aussprechen müssen!

Um die Messung der Feinstaubbelastung zu „unterstützen“ :smirk:, organisiert die Leipziger Linux User Group eine kleine Werkstatt zum Aufbau, Registrierung und Inbetriebnahme deines Feinstaubsensors.

<!--more-->

~~Am 24.04.2018 ab 19:00 Uhr gibt es dazu ein Vorbereitungstreffen im Rahmen des monatlichen Stammtisches in der~~

~~[Lutherburg](https://lutherburg-leipzig.de/)~~

Am 29.05.2018 ab 19:00 Uhr gibt es dazu ein Vorbereitungstreffen im Rahmen des monatlichen Stammtisches in der

[Gosenschenke](http://www.gosenschenke.de) im Vereinszimmer


{{< load-leaflet >}}

{{< leaflet-simple mapHeight="500px" mapWidth="100" mapLon="12.36734" mapLat="51.35765" markerLon="12.36734" markerLat="51.35765" markerContent="Gosenschenke 'Ohne Bedenken', Menckestraße 5, 04155 Leipzig" >}}

Das Vorbereitungstreffen soll dem Stand der Bestellungen dienen, der Terminfindung zum Aufbau im [Juli](/ankuendigung/werkstatt), dem Tausch von Doppelbestellungen bzw. Fehllieferungen und u.U. zusätzliche Sammelbestellungen aufgeben.

Für den Aufbau werden folgende Bauteile benötigt:

- NodeMCU (V2: 2,00€) oder (V3: 2,70€)
- SDS011 16,00€
- DHT22 2,05€
- flaches USB-Kabel 3m ca. 2,15€
- optinal: BMP180(0,70€) bzw BME280(2,40€)
- ein 5V USB-Netzteil, 500mA reicht

Wenn du dich vorher anmeldest, stellen wir zur Verfügung:

- Schrumpfschlauch als etwas stabilere Verbindungen für den DHT22 und SDS011
- Für den Sensor c.a. 20cm Schlauch mit 6mm Innendurchmesser.
- Als Gehäuse je zwei HT Bögen (DN 75 87°;)
- Zur Abdeckung gegen Getier und groben Schmutz zwei Stück feine Gardine (Durchmesser c.a. 10cm)
- Kabelbinder
- Käbelchen

An Werkzeug:

- ein Seitenschneider
- für stabilere Verbindungen Lötkolben, Lötzinn,
- Feuerzeug
- kleine Schraubenzieher

Weiter Infos finden sich [hier](https://luftdaten.info)

Anmeldung: <a href="mailto:vortrag@gaos.org">vortrag@gaos.org</a>
