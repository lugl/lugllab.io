---
date: "2020-10-29T11:51:45+01:00"
title: "CO2 Ampel"
authors: ["mr"]
emoji: true
categories:
  - Ankündigung
tags:
  - Corona
---
# Wir basteln wieder

Diesmal an einer CO2-Ampel :vertical_traffic_light:
https://www.heise.de/select/make/2020/5/2022015381334973804
oder werden wir bald Heimwerken :mask:

Projektablauf und Status:


1. Sammlung der "Interessensbekundung"
https://nuudel.digitalcourage.de/Fefg8uhm6EApDPlz lief bis 16.10.2020  
:green_heart: abgeschlossen

2. Geld einsammeln -> Überweisung  
:yellow_heart: da fehlt noch was :worried:

3. Sammelbestellung aufgeben -> je nach Anzahl beim Chinamann oder ...?  
:yellow_heart: da geht noch was

4. Abstimmung zur Software -> Stammtisch  
:red_circle:  da muss was kommen

5. gemeinsamer Zusammenbau -> Termin u.U. in der dezentrale finden  
:red_circle: 
6. Software aufspielen und einrichten ;-)  
:red_circle: 
7. prüfen/einmessen -> Profigerät  
:red_circle: 

Schreib uns eine Mail wenn du noch einsteigen willst vortrag@gaos.org