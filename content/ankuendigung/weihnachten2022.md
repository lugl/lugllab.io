---
date: "2022-12-14T19:50:23+01:00"
title: "Weihnachtsfeier und Wichteln 2022"
authors: ["mr"]
draft: false
emoji: true
categories:
  - Ankündigung
tags:
  - Weihnachtsfeier
  - Wichteln
---

### 2022 – Dezember - Stammtisch + Wichteln 


Das Jahr neigt sich dem Ende entgegen und die Feiertage stehen vor der Tür.
Geschenke verpacken muss natürlich geübt werden und dazu bietet sich unser Dezember-Stammtisch wieder an. Ja, das _Wichteln_ steht vor der Tür und so treffen wir uns

am	20. Dezember 2022,	um	19:00 Uhr 	zum Stammtisch

und um gegenseitig unsere Verpackungskünste zu präsentieren.
Wenn du keine „Schleife“ binden kannst, die wir bewundern können, ist es auch o.k. bitte schaue trotzdem vorbei.

![Linie](/images/Linie20.gif)

HowTo - Du packst, was immer du an PC oder sonstige IT Hardware abgeben kannst, möglichst festlich ein. Es gibt nur eine Bedingung, was immer du einpackst, muss noch einsatzfähig sein.
Der Ablauf sieht dann so aus: Auf dem Gabentisch liegt ein Abreißblock mit doppelten Nummern und daneben steht unser Stammtisch-Aufsteller. Du heftest einen Zettel mit der Nummer an deine Gabe und legst den anderen Zettel mit der gleichen Nummer in unseren Stammtisch-Aufsteller.

![Los](/images/lose.jpeg)
Merke dir diese Nummer! 

Nach dem Essen zieht dann jeder ein Los aus unserem Aufsteller, vergleicht die Nummer auf dem Zettel mit der Nummer in seinem Speicher, den ja jeder auf der Schulter trägt und wenn Los- und Speichernummer ≠ behält er dieses Los. Sonst zieht er ein weiteres Los und legt das Los mit der Nummer = Speichernummer zurück in unseren Stammtisch-Aufsteller. Dies geht dann reihum.
Mit der Losnummer ≠ Speichernummer greift sich einer nach dem anderen das Päckchen mit der entsprechenden Nummer und zeigt uns beim Auspacken hoffentlich sein strahlendes Gesicht. ;-) 

