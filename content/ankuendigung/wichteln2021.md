---
date: "2021-11-23T14:38:06+01:00"
title: "Dezemberstammtisch und Wichteln 2021"
authors: ["mr"]
draft: false
emoji: true
categories:
  - Ankündigung
  - Party
tags:
- Corona
- Stammtisch
- Wichteln
draft: false
---
Hallo Leipziger Linux User,

nun ist es so gekommen, wie es kommen sollte. Das Jahresende 2021 wird vom [Corona Virus gestaltet](https://www.leipzig.de/jugend-familie-und-soziales/gesundheit/neuartiges-coronavirus-2019-n-cov/).

Für unsere Treffen ergeben sich folgende Konsequenzen. Der Stammtisch am letzten Donnerstag im Monat in der Lutherburg fällt bis auf weiteres aus. Ein Treffen zwischen 19:00Uhr und 20:00Uhr gem. der aktuellen Corona-Verordnung lohnt sich nicht wirklich. Wir werden uns aber, wie schon zuvor, virtuell also im Netz sehen und sprechen können.

<DIV align="center">
#### LUG-L Stammtisch bei [Jitsi](https://meet.esemos.de/lug-l), Donnerstag, 25. November, 19.00Uhr
![Linie](/images/Linie20.gif)
</DIV>
Nun zu dem wichtigsten, schönsten und ereignisreichsten Stammtisch des Jahres!

<DIV align="center">
#### ![Weihnachtstux](/images/weihnachtstux.jpeg) Das Wichteln im Dezember.
</DIV>

Nach Rücksprache mit unserem Wirt der Lutherburg besteht die Möglichkeit zum 2G Treffen Sonntag's von 11:00 – 14:30Uhr. Da die jetzige Corona-Schutz-Verordnung bis 12.12. gilt und eher mit Verschärfungen zu rechnen ist stehen uns im Grunde nur zwei Termine zur Auswahl. Der 5. oder der 12. Dezember.  

Auf Grund des ungewöhnlichen Termins, der kürze der Zeit, den reservierten Raum in der Lutherburg wieder freizugeben und der verbleibenden Vorbereitungszeit für unseren Wirt bitte ich euch dringend bis kommenden Sonntag, 28. November an der fofenden Abstimmung teil zu nehmen. Wer sich also einen

<DIV align="center">
#### IT - Hardware – Schrott – Wichtel - Frühschoppen
</DIV>

vorstellen kann, [hier](https://nuudel.digitalcourage.de/5s8cKMcA3ZKZZbOP) geht es lang.

<DIV align="center">
![Linie](/images/Linie20.gif)
</DIV>

PS: für alle die Wicheln nicht kennen → [Weihnachten2018](ankuendigung/weihnachten2018)