---
date: "2018-06-12T17:21:39+02:00"
title: "Feinstaubsensor im Eigenbau – Werkstatt-Termin steht"
authors: ["mr"]
draft: false
emoji: true
categories:
  - Ankündigung
tags:
  - Feinstaubsensor
  - Workshop
---

Es ist so weit, wir haben einen Termin gefunden, an dem die Lötkolben glühen werden. :wink:

<!--more-->

Unseren [Feinstaubsensor](/ankuendigung/feinstaub) werden wir ...

```
am    10. Juli 2018
ab    19:00 Uhr

in der  dezentrale
        Dreilindenstraße 19
        04177 Leipzig
```
https://www.openstreetmap.org/way/41760585

... zusammenbauen, zusammenlöten, gemeinsam die Software aufspielen, den Sensor konfigurieren, die Messstation anmelden und registrieren.


1000 Dank an die „[Dezentrale e.V.](https://dezentrale.space)“ für die Möglichkeit, in ihren Räumlichkeiten zu arbeiten.

| Geolocation als QR-Code       | Termin als QR-Code |
| ------------- | ------------- |
| ![Geolocation als QR-Code](/images/GEO-Location-Dezentrale.jpg) | ![Termin als QR-Code](/images/Werkstatttermin.jpg) |
