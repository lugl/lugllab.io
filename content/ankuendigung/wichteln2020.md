---
date: "2021-05-30T12:23:31+02:00"
title: "Nachholtermin: Wichteln 2020"
authors: ["mr"]
draft: false
emoji: true
categories:
  - Ankündigung
  - Party
tags:
- Corona
- Stammtisch
- Wichteln
draft: false
---
Nein, natürlich nicht aber wie können wir sonst feiern 😂, dass die [7-Tage Inzidenz](https://www.leipzig.de/jugend-familie-und-soziales/gesundheit/neuartiges-coronavirus-2019-n-cov/#c228173) in Leipzig beständig unter 50 liegt? DANKE LEIPZIG!

![Inzidenz Leipzig](/images/20210607_Inzidenz_Leipzig_RKI.png)

Vorbehaltlich eines Wertes <50, können wir uns nach 8 Monaten

**am Donnerstag**

**den 24.06.2021**

**ab 19:00 Uhr**

zum **Stammtisch in der [Lutherburg](https://lutherburg-leipzig.de)** treffen. *Ich glaube mir zittern gerade die Hände 😉*

Und ja, der Tag hat sich geändert. Die Lutherburg hat bis auf weiteres montags und dienstags Ruhetag und da der letzte Mittwoch im Monat nicht immer frei wäre treffen wir uns, bis auf weiteres, am letzten Donnerstag im Monat. [Hier](/2021.06.24_Stammtisch.ics) findet ihr die Termin-Datei für euren Kalender.

😷Für das Treffen haben wir, die zu diesem Zeitpunkt geltenden [Verordnungen und Verfügungen](https://www.leipzig.de/jugend-familie-und-soziales/gesundheit/neuartiges-coronavirus-2019-n-cov/#c228167) einzuhalten. Es ist davon auszugehen, dass in geschlossenen Räumen weiterhin das Abstandsgebot gilt. Darüber hinaus wird „eine Registrierung“ zur Kontaktnachverfolgung bestehen bleiben. Unser Wirt wird sowohl die „Zettelwirtschaft“ mit Klarnamen usw. als auch das „[Event Check-in](https://www.coronawarn.app/de/eventregistration/)“  der [Corona Warn App](https://www.coronawarn.app) anbieten.

Für Smartphones mit Android und besonders für die Google freien Android Handys bietet sich die [App CCTG (Corona Contact Tracing Germany)](https://codeberg.org/corona-contact-tracing-germany/cwa-android) an. Diese findet sich in [F-Droid](https://f-droid.org/de/). Wer F-Droid noch nicht nutzt oder die Paketquelle der CCTG Entwickler hinzufügen will (empfohlen), findet [hier](https://bubu1.eu/cctg/) eine Beschreibung. Warum auch Du die CWA bzw. CCRG nutzen solltest, beschreibt [dieser Artikel.](https://krautreporter.de/3888-warum-du-die-corona-warn-app-spatestens-jetzt-installieren-solltest?shared=75c68f00-3855-4df5-8e1f-f59ed398b28f)

**Hinweis:** Die Apps CWA und CCTG zeigen nach der Installation den Risikostatus zunächst in grau an. Erst nach 5 bis 7 Tagen gelegentlich auch erst nach 10 bis 14 Tagen wechselt die Statusanzeige zu grün. Wenn der Risikostatus grün anzeigt mach der Check-in erst Sinn.

🙈 🙉 🙊

Noch eine Bitte: Um unserem Wirt die Planung zu erleichtern, tragt euch bis zum 23.06. in diese [unverbindliche Teilnehmerliste](https://nuudel.digitalcourage.de/dvkQNOK1gCLRQEME) ein.😸

___


##### Off-topic / fyi / #zwinkersmiley
Wer sich mit der „Luca“ App konfrontiert sieht, sollte den Worten [dieses Beitrags](https://www.heise.de/forum/heise-online/Kommentare/Luca-first-Bedenken-second-Pandemiebekaempfung-mit-lueckenhafter-Software/Lucia-App-auf-F-Droid/posting-38778885/show/) im heise-Forum folgen.
