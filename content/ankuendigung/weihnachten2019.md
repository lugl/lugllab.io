---
date: "2019-11-27T10:41:30+01:00"
title: "Weihnachten 2019"
authors: ["mr"]
categories:
  - Ankündigung
tags:
  - Weihnachtsfeier
  - Wichteln
draft: false
---
### Dezember Stammtisch = Wichteln ... ?

Dienstag, 17. Dezember 2019
19.00 Uhr in der Lutherburg

Wie schon in den beiden Vorjahren so wollen wir auch in diesem Jahr wieder Weihnachtswichteln. Da sich nichts zu den beiden Vorjahren verändert hat verweisen wir einfach mal auf den [Beitrag des Vorjahres](/ankuendigung/weihnachten2018/)


Jeder ist herzlich eingeladen auch ohne "Gabe".