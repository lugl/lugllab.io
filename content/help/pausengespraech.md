---
date: "2018-11-20T16:52:26+01:00"
title: "Pausengespräch"
authors: ["mr"]
categories:
  - Help
tags:
  - Datenschutz
draft: false
---

{{< convo sep=":" >}}

Alice ::  So bin jetzt weg von Twitter!
Bob :: Und wo bist du sonst noch so angemeldet?
Alice :: Hier und da, aber das nutze ich nicht mehr.
Bob ::  Du weißt schon, dass du denn weiterhin deine Daten, dein Surfverhalten usw. übermittelst!
Alice :: Ja, aber das Abmelden ist so kompliziert und ich weiß nicht wie es geht.
Bob ::  Ja, das ist Absicht, mit Hilfe der „dark pattern Techniken“ wird es dir z.T. unmöglich gemacht deinen Account zu löschen. Nur bei ganz wenigen Accounts ist dies systembedingt wie z.B. Wikipedia.
Alice :: Ja, und was kann ich machen?
Bob ::  Schau mal auf die Seite [backgroundchecks](https://backgroundchecks.org/justdeleteme/de.html) . Sie hilft weiter.
Alice :: Und in Zukunft, was mache ich da?
Bob ::  Erste Regel, sparsam mit Informationen über dich sein und nutze das 3-Browser-Konzept.
Bob ::  Da die Pause ist rum, melde dich ein anderes Mal wenn du weitere Fragen hast.
{{< /convo >}}

![3-browser-konzept](https://media.kuketz.de/blog/artikel/2015/not-my-data/3-browser-konzept.jpg)

Am 27.11. , 19:00 Uhr [trifft](https://gaos.org/page/treffen/) sich die Leipziger Linux User Group wieder in der Lutherburg, Wittenberger Straße 26 in Leipzig. Für Fragen stehen wir gerne zur Verfügung.


Auch in deiner Nähe findet sich eine [Community](https://gaos.org/page/link/) rund um Linux und Open-Source-Software
